import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'mysql://square2_admin:123456@localhost/square2'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
