from app import db


class Answer(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	sum = db.Column(db.Integer, unique=True)
	x = db.Column(db.Integer)
	y = db.Column(db.Integer)
