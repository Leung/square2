from app import app, db
from .models import Answer


def insert(x, y, sum):
	ans = Answer(x=x, y=y, sum=sum)
	db.session.add(ans)
	db.session.commit()


@app.route('/<sum>', methods=['GET'])
@app.route('/index/<sum>', methods=['GET'])
def index(sum):
	sum = int(sum)
	ans = Answer.query.filter_by(sum=sum).first()
	if ans is not None:
		return ("x = %d, y = %d" % (ans.x, ans.y))

	for x in range(1, sum):
		for y in range(1, sum):
			if x + y == sum:
				insert(x, y, sum)
				return ("x = %d, y = %d" % (x, y))


@app.route('/about', methods=['GET'])
def about():
	return "Let's make something interesting."
